<?php
defined('TYPO3_MODE') || die();

(function () {
  \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('mmc_directmail_subscription', 'Configuration/TypoScript', 'MMC directmail subscription');
})();
