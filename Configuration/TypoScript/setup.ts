
plugin.tx_mmcdirectmailsubscription {
	view {
		templateRootPaths.10 = {$plugin.tx_mmcdirectmailsubscription.view.templateRootPath}
		partialRootPaths.10 = {$plugin.tx_mmcdirectmailsubscription.view.partialRootPath}
		layoutRootPaths.10 = {$plugin.tx_mmcdirectmailsubscription.view.layoutRootPath}
	}
	settings{
		scriptPath = {$plugin.tx_mmcdirectmailsubscription.settings.scriptPath}
		keepEmailAddressUnique = {$plugin.tx_mmcdirectmailsubscription.settings.keepEmailAddressUnique}
		fromEmail = {$plugin.tx_mmcdirectmailsubscription.settings.fromEmail}
		fromName = {$plugin.tx_mmcdirectmailsubscription.settings.fromName}
		removeAddress = {$plugin.tx_mmcdirectmailsubscription.settings.removeAddress}
	}
	persistence {
		storagePid = {$plugin.tx_mmcdirectmailsubscription.persistence.storagePid}
	}
}

# model mapping
config.tx_extbase.persistence.classes {
	MMC\MmcDirectmailSubscription\Domain\Model\Address {
		mapping{
			tableName = tt_address
		}
	}
}

[globalVar = LIT:1 = {$plugin.tx_mmcdirectmailsubscription.settings.includeJQuery} ]
	page.includeJSLibs.tx_mmcdirectmailsubscription_jquery = {$plugin.tx_mmcdirectmailsubscription.settings.scriptPath}jquery-3.5.1.min.js
[global]
page.includeJS.tx_mmcdirectmailsubscription = {$plugin.tx_mmcdirectmailsubscription.settings.scriptPath}mmc_dmailsubscr-0.9.6.js


plugin.tx_mmcdirectmailsubscription._CSS_DEFAULT_STYLE (
  .tx-mmc-directmail-subscription .form-row label{ display:block }
  .tx-mmc-directmail-subscription .form-row.error label{ color:#D00 }
  .tx-mmc-directmail-subscription .form-row.error input,
  .tx-mmc-directmail-subscription .form-row.error textarea{
  	color:#D00;
  	border-bottom: 1px dotted #D00;
  }
	.tx-mmc-directmail-subscription input.h { display:none }
)
