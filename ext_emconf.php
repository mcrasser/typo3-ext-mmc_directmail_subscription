<?php

$EM_CONF[$_EXTKEY] = [
	'title' => 'MMC directmail subscription',
	'description' => 'Custom direct-mail subscription plugin',
	'category' => 'plugin',
	'author' => 'Matthias Mächler',
	'author_email' => 'maechler@mm-computing.ch',
	'state' => 'stable',
	'uploadfolder' => false,
	'version' => '2.1.0',
	'constraints' => [
		'depends' => [
			'typo3' => '9.5.19-10.4.99',
			'tt_address' => '5.1.2-5.99.99',
			'direct_mail' => '6.0.1-7.99.99'
		],
		'conflicts' => [],
		'suggests' => []
	]
];
