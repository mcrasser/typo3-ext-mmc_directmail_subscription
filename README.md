# TYPO3-Extension *mmc_directmail_subscription*

## What does it do?

This is a simple replacement for the direct_mail_subscription extension. It's buit on extbase, so translations and templating is much easier. To prevent from spam, 'honeypot' instead of captcha is used.

It allows FE-users to

* Subscript to the newsletter (user receives a mail to confirm his email-address)
* Cancel newsletter subscription (user receives a mail to confirm his email-address)

Supported tt_address fields are

* gender
* firstName
* lastName
* name (will be set to 'firstName lastName' by default)
* moduleSysDmailHtml (by default set to 1 by a hidden field in the registration form)

By default, on subscription all existing records with identical email-adresses will be removed. This option can be disabled by TS-constant.

## Configuration Reference
Include the static template of the extension in your site template

### Typoscript Constants:
**keepEmailAddressUnique (default: true)**  
Remove tt_address records with the same email-address on subscription.

**includeJQuery (default: true)**  
Include jQuery library: disable if jQuery is already available.

**removeAddress (default: true)**  
Remove email-address on cancel subscription: remove tt_address records on cancle subscription. disable to just hide them.

**fromEmail**  
Email sender address for address-verification-emails. If not set, no mails will be sent!

**fromName**  
Email sender name for address-verification-emails

**scriptPath**  
Path to Scripts (default: typo3conf/ext/mmc_directmail_subscription/Resources/Public/Scripts/)

### Storage for tt_address
* 'Default storage PID' of the extension
* 'Record Storage Page' of the FE-Plugin

## ChangeLog

### 2.1.0
update dependencies to enable direct_mail 7.0.0^

### 2.0.3
update this changelog :-)

### 2.0.2
fix extension dependencies

### 2.0.1
upgraded jquery core to 3.5.1  
include js via typoscript-setup, use includeJSLibs for jQuery inclusion

### 2.0.0
TYPO3 9/10 compatibility; drop TYPO3 8 support

### 1.3.1
Trigger packagist

### 1.3.0
* Add composer support
* Move documentation to this file
* Change filenames constants.txt / setup.txt to \*.ts
* Drop TYPO3 6/7 support

### 1.2.0
* TYPO3 8 compatibility
* language labels it: "Grazie Mille" instead of "Molte Grazie"

### 1.1.3
updated changelog :-)

### 1.1.2
multiple view-paths (for TYPO3 7) in typoscript-setup

### 1.1.1
Fixed minimal version for direct_mail to 4.1.0

### 1.1.0
Compatibility to TYPO3 7.6

### 1.0.0
* Modified es.locallang.xlf
* Changed state form "beta" to "stable"

### 0.9.9
es.locallang.xlf formating

### 0.9.8
* [FEATURE] Contribution by Benjamin Einert [b.einert(at)abija.de]: Option to hide address on unsubscript
* [BUGFIX] Error when record is already deleted when following email confirmation link
* Updated Documentation

### 0.9.7
Contributions by Loek Hilgersom [loek(at)netcoop.nl]: - [BUGFIX] Set actions register and cancel as non-cacheable actions to make them work in production context - Generate cleaner back links directly to current page without parameters because register is default action - [SECURITY] Set no-cache headers as soon as any personal data is passed to the view to avoid caching of personal data

### 0.9.6
* added FE-language labels for Dutch (nl). Thanks to Stefan Rosbak (srosbak@zigwebsoftware.nl)
* simplified honeypot implementation
If you copied and customized the template, you have to consider change for the hidden honeypot fields!

### 0.9.5
email repeat check in forms
