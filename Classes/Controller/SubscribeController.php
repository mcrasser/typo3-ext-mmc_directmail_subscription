<?php
namespace MMC\MmcDirectmailSubscription\Controller;

use \MMC\MmcDirectmailSubscription\Domain\Repository\AddressRepository;
use \MMC\MmcDirectmailSubscription\Domain\Model\Address;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Matthias Mächler <maechler@mm-computing.ch>, mm-computing.ch
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
* SubscribeController
*/
class SubscribeController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

	/**
	 * addressRepository
	 *
	 * @var \MMC\MmcDirectmailSubscription\Domain\Repository\AddressRepository
	 */
	protected $addressRepository = NULL;

	/**
	 * currently loaded address, for use in child classes
	 *
	 * @var \MMC\MmcDirectmailSubscription\Domain\Model\Address
	 */
	protected $address = NULL;


	public function __construct(AddressRepository $addressRepository){
		$this->addressRepository = $addressRepository;
	}

	/**
	* action register
	* register address -> needs email verification to unhide record
	*
	* @param \MMC\MmcDirectmailSubscription\Domain\Model\Address $address
	* @param string $h1 honeypot field, must be empty
	* @return void
	*/
	public function registerAction( $address = NULL, $h1 = '.') {
		if( $address && ($h1 == '') ){
			$address->setHidden( true );
			$address->setEmailVerificationCode( $this->generateVerificationString( 16 ) );
			$this->addressRepository->add( $address );
			$this->addressRepository->persistAll();
			$this->sendRegisterVerificationMail( $address );
		}	else $address = NULL;
		// set $this->address for use in child classes
		$this->address = $address;
		// assign to view, if NULL, view displays the register form, else a sent-mail notification
		$this->view->assign( 'address', $address );
		$this->setNoCacheHeader($address);
	}

	/**
	* action registerConfirm
	*
	* @param string $addressUid
	* @param string $evc email verification code
	* @return void
	*/
	public function registerConfirmAction( $addressUid = NULL, $evc = NULL ) {
		// verificate address..
		if( $address = $this->verificateAddress( $addressUid, $evc ) ){
			// remove records with identical email address..
			if( $this->settings['keepEmailAddressUnique'] )
				$this->addressRepository->removeAllOthersWithIdenticalEmail( $address );
			// confirmed, unhide & store..
			$address->setHidden( false );
			$this->addressRepository->update( $address );
			// persist
			$this->addressRepository->persistAll();
		}
		// set $this->address for use in child classes
		$this->address = $address;
		// assign to view, if NULL, view displays a fail message
		$this->view->assign( 'address', $address );
		$this->setNoCacheHeader($address);
	}


	/**
	* action cancel
	*
	* @param string $email
	* @param string $h1 honeypot field, must be empty
	* @return void
	*/
	public function cancelAction( $email = NULL, $h1 = '.' ) {
		$address = NULL;
		$this->addressRepository->setRespectHidden( true ); // don't look for hidden records
		if(
			$email &&
			($h1 == '') &&
			($address = $this->addressRepository->findOneByEmail( $email ) )
		){
			// address found, set email verification code and send mail..
			$address->setEmailVerificationCode( $this->generateVerificationString( 16 ) );
			$this->addressRepository->update( $address );
			$this->addressRepository->persistAll();
			$this->sendCancelVerificationMail( $address );
		}
		// set $this->address for use in child classes
		$this->address = $address;
		/* assign to view
		 * email is NULL: view displays cancel form
		 * email is not NULL, address is not NULL: view displays sent-mail confirmation
		 * email is not NULL, address is NULL: view displays email-address not found message
		 */
		$this->view->assign( 'email', $email );
		$this->view->assign( 'address', $address );
		$this->setNoCacheHeader($address);
	}

	/**
	* action cancelConfirm
	*
	* @param string $addressUid
	* @param string $evc email verification code
	* @return void
	*/
	public function cancelConfirmAction( $addressUid = NULL, $evc = NULL ) {
		// verificate address..
		if( $address = $this->verificateAddress( $addressUid, $evc ) ){
			// confirmed, remove address record
			if( $this->settings['removeAddress'] ) {
				$this->addressRepository->remove( $address );
			}else{
				$address->setHidden( true );
				$this->addressRepository->update( $address );
			}
			$this->addressRepository->persistAll();
		}
		// set $this->address for use in child classes
		$this->address = $address;
		// assign to view, if NULL, view displays a fail message
		$this->view->assign( 'address', $address );
		$this->setNoCacheHeader($address);
	}

	/* ----------------------------- email verification ---------------------------------------*/

	/**
	* verificateAddress
	*
	* @param integer $addressUid
	* @param string $evc email verification code to check address record against
	* @return \MMC\MmcDirectmailSubscription\Domain\Model\Address $address NULL if invalid
	*/
	protected function verificateAddress( $addressUid, $evc ){
		if( $addressUid && $evc ){
			// avoid property override by arguments (necessary?), reload original record..
			if( $address = $this->addressRepository->findOneByUid( $addressUid ) ){
				// check verification code
				if( $address->getEmailVerificationCode() != $evc )
					$address = NULL;
			}
		} else
			$address = NULL;
		// return
		return $address;
	}

	/**
	* generateVerificationString
	*
	* @param int $length of the random string
	* @return string $verificationString
	*/
	protected function generateVerificationString( $length ){
		$chrs = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$chrsLen = strLen( $chrs );
		$vstr = '';
		for( $i = 0; $i < $length; $i++ )
			$vstr .= $chrs[rand(0, $chrsLen - 1)];
		return $vstr;
	}

	/**
	* sendRegisterVerificationMail
	*
	* @param \MMC\MmcDirectmailSubscription\Domain\Model\Address $address
	* @return void
	*/
	protected function sendRegisterVerificationMail( $address ){
		$this->sendVerificationMail(
			$this->translate('mail.register.subject'),
			$address,
			'Subscribe/RegisterMail.html'
		);
	}

	/**
	* sendCancelVerificationMail
	*
	* @param \MMC\MmcDirectmailSubscription\Domain\Model\Address $address
	* @return void
	*/
	protected function sendCancelVerificationMail( $address ){
		$this->sendVerificationMail(
			$this->translate('mail.cancel.subject'),
			$address,
			'Subscribe/CancelMail.html'
		);
	}

	/**
	* sendVerificationMail
	*
	* @param string $subject the email subject
	* @param \MMC\MmcDirectmailSubscription\Domain\Model\Address $address
	* @param string $bodyTemplateFile the template file for the mail body
	* @return void
	*/
	protected function sendVerificationMail( $subject, $address, $bodyTemplateFile ){
		// make mailer instance
		$mail = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Mail\\MailMessage');
		// setup email components
		$recipients = array( $address->getEmail() => $address->getName() );
		$from = array( $this->settings['fromEmail'] => $this->settings['fromName'] );
		$view = $this->getStandaloneView( $bodyTemplateFile );
		$view->assign( 'address', $address );
		$mailBody = $view->render();
		// send mail
		try{
			$mail->setFrom( $from )->setTo( $recipients )->setSubject( $subject );
			if( method_exists($mail, 'html') ){
		    $mail->html($mailBody); // TYPO3 10
		  } else {
		    $mail->setBody($mailBody, 'text/html'); // TYPO3 9
		  }
			$mail->send();
		} catch ( \Exception $e ){
			$this->addFlashMessage('error while sending mail: '.$e->getMessage() );
		}
		if(! $mail->isSent())
			$this->addFlashMessage('error while sending mail: mail could not be sent' );
	}

	/* ------------------------------------- Utility ---------------------------------------- */


	protected function translate( $key ){
		return \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate( $key, 'MmcDirectmailSubscription' );
	}

	/**
	 * Creates a standaloneView instance with given template-file and controller-settings
	 * @param string $file Path below Template-Root-Path
	 * @return \TYPO3\CMS\Fluid\View\StandaloneView Fluid-Template-Renderer
	 */
	private function getStandaloneView($file) {
	  // create another instance of Fluid
	  $renderer = $this->objectManager->get('TYPO3\\CMS\\Fluid\\View\\StandaloneView');
    // set controller context and template
    $renderer->setControllerContext($this->controllerContext);
    $this->setViewConfiguration( $renderer );
	  $renderer->setTemplate($file);
	  // and return the new Fluid instance
	  return $renderer;
	}

	/**
	 * Set no-cache headers as soon as the view receives any data (address) that should not get cached somewhere on the road
	 *
	 * @return void
	 */
	protected function setNoCacheHeader($address) {
		if ($address) {
			//send no-cache header
			// HTTP 1.1
			header('Cache-Control: no-cache, must-revalidate');
			// HTTP 1.0
			header('Pragma: no-cache');
			// robots
			header('X-Robots-Tag: noindex, nofollow');
		}
	}

}
